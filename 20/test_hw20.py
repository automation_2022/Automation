import pytest
import requests


@pytest.fixture(scope='session', autouse=True)
def resource(request):
    print("setup")

    def teardown():
        print("teardown")
    request.addfinalizer(teardown)
    return resource


@pytest.fixture(scope='session')
def request_provider():
    def provide_request(id, name, category_id, category_name, photoUrls, tag_id, tag_name, status):
        return {
            "id": id,
            "name": name,
            "category": {
                "id": category_id,
                "name": category_name
            },
            "photoUrls": [
                 photoUrls
            ],
            "tags": [
                {
                    "id": tag_id,
                    "name": tag_name
                }
            ],
            "status": status
            }
    return provide_request


@pytest.fixture(scope='session')
def request_headers():
    return {
        "accept": "application/xml",
        "Content-Type": 'application/json'
        }


@pytest.fixture(scope='function')
def request_get_status_url():
    return "https://petstore3.swagger.io/api/v3/pet/findByStatus"


@pytest.fixture(scope='function')
def request_post():
    return "https://petstore3.swagger.io/api/v3/pet"


@pytest.mark.parametrize('status',
                         [('available',
                          'pending',
                          'sold')])
def test_get_status(request_get_status_url, request_headers, status):
    params = {"status": status}
    response_get_status = requests.get(request_get_status_url, params=params, headers=request_headers)
    assert response_get_status.status_code == 200


@pytest.mark.parametrize(("id", "name", "category_id", "category_name", "photoUrls", "tag_id", "tag_name", "status"),
                         [(23, "Patron", 1, "Dogs", "string", 0, "string", "available")])
def test_create_pet(request_provider,
                    request_headers,
                    request_post,
                    id,
                    name,
                    category_id,
                    category_name,
                    photoUrls,
                    tag_id,
                    tag_name,
                    status):
    api_request = request_provider(id=id, name=name, category_id=category_id, category_name=category_name, photoUrls=photoUrls, tag_id=tag_id, tag_name=tag_name,  status=status)
    response = requests.post(url=request_post, json=api_request, headers=request_headers)
    assert response.status_code == 200


@pytest.mark.parametrize(("id", "name", "category_id", "category_name", "photoUrls", "tag_id", "tag_name", "status"),
                         [(23, "Stepan", 2, "Cats", "string", 0, "string", "pending")])
def test_update_pet(request_provider,
                    request_headers,
                    request_post,
                    id,
                    name,
                    category_id,
                    category_name,
                    photoUrls,
                    tag_id,
                    tag_name,
                    status):
    api_request_2 = request_provider(id=id, name=name, category_id=category_id, category_name=category_name, photoUrls=photoUrls, tag_id=tag_id, tag_name=tag_name, status=status)
    response_2 = requests.put(url=request_post, json=api_request_2, headers=request_headers)
    assert response_2.status_code == 200



