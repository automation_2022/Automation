#  Task 1. Створити клас Vehicle (транспортний засіб):
# ні від чого не наслідується
# в ініціалізатор класу (__init__ метод) передати
# producer: str
# model: str
# year: int
# tank_capacity: float # обєм паливного баку
# tank_level: float = 0 # початковий параметр рівня паливного баку дорівнює 0,
# параметр в аргументах не передавати
# maxspeed: int
# fuel_consumption: float # litres/100km споживання пального
# odometer_value: int = 0 # при сході з конвеєра пробіг нульовий, параметр в аргументах не передавати
# визначити метод __repr__, яким повертати інформаційну стрічку (наповнення на ваш вибір,
# проте параметри model and year and odometer_value мають бути передані
# написати метод refueling, який не приймає жодного аргумента, заправляє автомобіль на уявній автозаправці
# до максимума (tank_level = tank_capacity), після чого виводить на екран повідомлення, скільки літрів було заправлено
# (перша заправка буде повного баку, а в інших випадках величина буде залежати від залишку пального в баку) ???
# написати метод race, який приймає один аргумент (не враховуючи self) - відстань, яку потрібно проїхати, а повертає словник, в якому вказано, скільки авто проїхало, з огляду на заповнення паливного баку перед поїздкою, залишок пального (при малому кілометражі все пальне не використається), та час, за який відбулася дана поїздка, з урахування, що середня швидкість складає 80% від максимальної (витрата пального рівномірна незалежно від швидкості)
# за результатами роботи метода в атрибуті tank_level екземпляра класа має зберігатися залишок пального
# після поїздки (зрозуміло, що не менше 0)
# збільшити на величину поїздки показники odometer_value
# написати метод lend_fuel, який приймає окрім self ще й other обєкт,
# в результаті роботи якого паливний бак обєкта, на якому було викликано відповідний метод,
# наповнюється до максимального рівня за рахунок відповідного зменшення рівня пального у баку
# дружнього транспортного засобу
# вивести на екран повідомлення з текстом типу "Дякую, бро, виручив.
# Ти залив мені *** літрів пального"
# у випадку, якщо бак першого обєкта повний або у другого обєкта немає пального,
# вивести повідомлення "Нічого страшного, якось розберуся"
# написати метод get_tank_level, для отримання інформації про залишок пального конкретного інсттанса
# написати метод get_mileage, який поверне значення пробігу odometer_value
# написати метод __eq__, який приймає окрім self ще й other обєкт (реалізація магічного методу для оператора порівняння == )
# даний метод має повернути True у випадку, якщо 2 обєкта, які порівнюються, однакові за показниками року випуску та пробігу
# (значення відповідних атрибутів однакові, моделі можуть бути різними)
# створіть не менше 2-х обєктів класу, порівняйте їх до інших операцій, заправте їх, покатайтесь на них на різну відстань,
# перевірте пробіг, позичте один в одного пальне, знову порівняйте

class Vehicle:
    def __init__(self,
                producer:str,
                model:str,
                year:int,
                tank_capacity:float,
                maxspeed:int,
                fuel_consumption:float,
                tank_level=0.0,
                odometer_value=0):
        self.producer = producer
        self.model = model
        self.year = year
        self.tank_capacity = tank_capacity
        self.maxspeed = maxspeed
        self.fuel_consumption = fuel_consumption
        self.tank_level = tank_level
        self.odometer_value = odometer_value

    def __repr__(self):
        return f'{self.model}, {self.year}, {self.odometer_value}'

    def refueling(self):
        max_capacity = self.tank_capacity - self.tank_level
        self.tank_level += max_capacity
        print(f'Було заправлено {max_capacity} літрів пального')

    def race(self, distance):
        """

        :param distance: int
        :return: dict of distance, time, tank_level
        """
        max_distance = (self.tank_level / self.fuel_consumption) * 100
        if distance > max_distance:
            distance = max_distance
        self.tank_level -= (distance * self.fuel_consumption) / 100
        self.odometer_value += distance
        time = distance / (self.maxspeed * 0.8)
        info = {
            'time': time,
            'tank_level': self.tank_level,
            'distance': distance,
        }
        return info

    def lend_fuel(self, other):
        """
        :param other
        :return: other
        """

        if self.tank_level == self.tank_capacity or other.tank_level == 0:
            print('Нічого страшного, якось розберуся')
        else:
            max_quantity = self.tank_capacity - self.tank_level
            possible_quantity = max_quantity if other.tank_level >= max_quantity else other.tank_level
            self.tank_level += possible_quantity
            other.tank_level -= possible_quantity
            print(f'Дякую, бро, виручив. Ти залив мені {possible_quantity} літрів пального')

    def get_tank_level(self):
        return self.tank_level

    def get_mileage(self):
        return self.odometer_value

    def __eq__(self, other):
        """

        :param other
        :return: comparison of years and odometer values
        """
        return self.year == other.year and self.odometer_value == other.odometer_value


if __name__ == '__main__':
    bmw = Vehicle('BMW', 'X5', 2014, 50.0, 130, 13.5, 20)
    audi = Vehicle('Audi', 'A5', 2014, 30.0, 120, 14.2, 20)
    bmw.refueling()
    audi.refueling()
    print('Distance and time for BMW:', bmw.race(80))
    print('Distance and time for Audi:', audi.race(40))
    print('BMW tank level:', bmw.get_tank_level())
    print('Audi tank level:', audi.get_tank_level())
    bmw.lend_fuel(audi)
    audi.lend_fuel(bmw)
    print('BMW odomoter value:', bmw.get_mileage())
    print('Audi odomoter value:', audi.get_mileage())
    print('BMW and Audi was made in the same year and have equal odometer values:', bmw == audi)













