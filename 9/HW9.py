#  Завдання №1
# Розробити клас Людина. Людина має:
# Ім'я
# Прізвище
# Вік (атрибут але ж змінний)
# Стать
# Люди можуть:
# Їсти
# Спати
# Говорити
# Ходити
# Стояти
# Лежати
# Також ми хочемо контролювати популяцію людства. Змінювати популяцію можемо в __init__.
# Треба сказати, що доступ до статичних полів класу з __init__ не можу іти через НазваКласу.статичий_атрибут,
# позаяк ми не може бачити імені класу. Але натомість ми можемо сказати self.__class__.static_attribute.

import datetime


class Human:
    population = 0

    def __init__(self, name, surname,  gender, age=datetime.date.today()):
        """
        Init:
        :param name: str
        :param surname: str
        :param gender: str
        :param age: int
        """
        self.name = name
        self.surname = surname
        self.gender = gender
        self.age = age
        self.__class__.increase_population()

    def eat(self):
        print(f'{self.name} can eat')

    def sleep(self):
        print(f'{self.name} can sleep')

    def speak(self):
        print(f'{self.name} can speak')

    def walk(self):
        print(f'{self.name} can walk')

    def stand(self):
        print(f'{self.name} can stand')

    def lie(self):
        print(f'{self.name} can lie')

    @classmethod
    def increase_population(cls):
        print('New human was born')
        cls.population += 1

    @property
    def age_determination(self):
        return (datetime.date.today() - self.age).days // 365


person = Human('Anastasiia', 'Kosenko', 'female', age=datetime.date(1996, 3, 24))

Human.increase_population()

print("Person's name and surname:", person.name, person.surname)

print()

print("Person's age:", person.age_determination)

print()

print("Person's gender:", person.gender)

print()

print("Person's capabilities:")

person.eat(), person.sleep(), person.walk(), person.stand(), person.lie(), person.speak()




