#  Task 1.
#  Розробити функцію, котра приймає колекцію та обʼєкт функції, що приймає один аргумент.
#  Певернути колекцію, кожен член якої є перетвореним членом вхідної колекції.
#  Нотатка. Обʼєкт функції, яку передаємо вказує на функцію, котра приймає один аргумент.
#  Не користуватися функціями map чи filter!!!

import math

a = [1, 2, 3, 4]

math.factorial(8)

print([math.factorial(s) for s in a])

#  or


def get_function(function):
    return function


def my_function(*args, another_function):
    my_collection = [another_function(i) for i in args]
    return my_collection


print(my_function('first', 'second', 'third', another_function=get_function))

