# Task 1
# We have 3 vacation requests types:
# 1. Vacation
# 2. Sick leave
# 3. Day off
# Write a program, which will propose you to choose one of the vacation types,
# enter First Name, Surname, from_date and to_date.
# As a result, on the console should be displayed vacation request.
# Each vacation type consists from 2 parts:
# • Title (1 for all types. Listed below)
# • Vacation Request pattern (1 for each type. Listed below)
# Request should contain entered First Name and Surname.
# Title
# CEO Red Bull Inc.
# Mr. John Bigbull
# Vacation Type: Vacation Pattern
# Hi John,
# I need the paid vacations from from_date to to_date.
# First Name Surname
# Vacation Type: Sick Leave Pattern
# Hi John,
# I need the paid sick leave from from_date to to_date.
# First Name Surname
# Vacation Type: Day Off Pattern
# Hi John,
# I need the paid vacations from from_date to to_date.
# First Name Surname


def vacations(func):
    def wrapper():
        print('CEO Red Bull Inc.\nMr. John Bigbull')
        return func
    return wrapper()


def choice():
    first_name = input('Please, enter your first name: ')
    last_name = input('Please, enter your last name: ')
    from_date = input('Please, select start date: ')
    to_date = input('Please, select end date: ')
    selection = input('Choose a vacation type:' + '\n' + 'Vacations' + '\n' + 'Sick leave' + '\n' + 'Day off' + '\n')
    return first_name, last_name,  from_date, to_date, selection


first_name, last_name, from_date, to_date, selection = choice()


@vacations
def send_request():
    if selection == 'Vacations':
        print(f'Hi John, I need the paid vacations from {from_date} to {to_date}.\n{first_name} {last_name}')
    elif selection == 'Sick leave':
        print(f'Hi John, I need the paid sick leave from {from_date} to {to_date}.\n{first_name} {last_name}')
    elif selection == 'Day off':
        print(f'Hi John, I need the paid days off from {from_date} to {to_date}.\n{first_name} {last_name}')


if __name__ == '__main__':
    send_request()