def input_number(message):
    """
    Function for input_number
    :param message: int
    :type message: int
    :return: variable userinput
    """
    while True:
        try:
            userinput = int(input(message))
        except ValueError:
            print("Введіть ціле число")
            continue
        else:
            return userinput


def year_word(word):
    """
    Function for year_word
    :param word: int
    :type word: int
    :return: variable years[result]
    """
    years = ['рік', 'роки', 'років']
    if word % 10 == 1 and word % 100 != 11:
        result = 0
    elif 2 <= word % 10 <= 4 and (word % 100 < 10 or word % 100 >= 20):
        result = 1
    else:
        result = 2
    return years[result]


def client_age(age):
    """
    Function for client_age
    :param age: int
    :type age: int
    :return: age
    """
    year = year_word(age)
    if age > 0 and age % 10 == age // 10 % 10:
        print(f'О, Вам {age}', year + '.', 'Який цікавий вік!')
    elif 0 < age < 7:
        print(f'Тобі ж {age}', year + '!', 'Де твої батьки?')
    elif 0 < age < 16:
        print(f'Тобі лише {age}', year + ',', 'а це фільм для дорослих!')
    elif age > 65:
        print(f'Вам {age}', year + '?', 'Покажіть пенсійне посвідчення!')
    else:
        print(f'Незважаючи на те, що вам {age}', year + ',', 'білетів всеодно нема!')
    return age