from user import User

users_data = {}
with open('users.csv', 'r') as csv_file:
    reader = csv_file.readlines()
    users_data['headers'] = reader.pop(0)
    for row in reader:
        users_data[row[0]] = User(row[0], row[1], row[2], row[3])

