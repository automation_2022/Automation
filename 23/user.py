from __future__ import annotations
from gender import Gender


class User:
    def __init__(
            self,
            first_name: str,
            last_name: str,
            email: str,
            gender: Gender
    ) -> None:
        self.__first_name = first_name
        self.__last_name = last_name
        self.__email = email
        self.__gender = gender

    @property
    def first_name(self):
        return self.__first_name

    @property
    def last_name(self):
        return self.__last_name

    @property
    def email(self):
        return self.__email

    @property
    def gender(self):
        return self.__gender

    @classmethod
    def from_csv(cls, csv_data: dict) -> User:
        return cls(**csv_data)








