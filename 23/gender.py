from enum import Enum, unique


@unique
class Gender(Enum):
    MALE = 'Male'
    FEMALE = 'Female'

    @classmethod
    def gender_method(cls, definition):
        if isinstance(definition, cls):
            definition = definition.value
        if not definition in cls.__members__:
            return False
        else:
            return True


class GenderDict(dict):

    def __setitem__(self, key, value):
        if Gender.gender_method(key):
            super().__setitem__(Gender(key), value)
        else:
            raise KeyError(f"Gender {key} is not valid")

    def __getitem__(self, key):
        if isinstance(key, str):
            gender_item = Gender(key.upper())
        return super().__getitem__(gender_item)


GenderDict()
