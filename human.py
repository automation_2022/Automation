import datetime
from dataclasses import dataclass
from type_gender import GenderDefinition

@dataclass
class Human:
    name: str
    surname: str
    position: str
    age = datetime.date.today()

    @property
    def age_determination(self):
        return (datetime.date.today() - self.age).days // 365

    def claim_humans_gender(self):
        print(f"{Human.position} are {GenderDefinition.FEMALE} and {GenderDefinition.MALE}")




