import unittest
from game import Game
from except_file_for_game import NegativeDice, BigDice


class MyTestSuite(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.game_round = Game

    @classmethod
    def tearDownClass(cls):
        del cls.game_round

    def setUp(self):
        self.game_rules = Game

    def tearDown(self):
        del self.game_rules

    def test_normal(self):
        first_normal_scores = 2
        second_normal_scores = 3
        third_normal_scores = 1
        expected_normal_scores = 6
        start = Game()
        score = start.throw(first_normal_scores, second_normal_scores, third_normal_scores)
        self.assertTrue(score == expected_normal_scores)

    def test_three_dices(self):
        first_pair = 3
        second_pair = 3
        third_pair = 3
        start_2 = Game()
        score_2 = start_2.throw(first_pair, second_pair, third_pair)
        self.assertEqual(score_2, 300)


    def test_pair(self):
        first = 2
        second = 1
        third = 2
        start_3 = Game()
        score_3 = start_3.throw(first, second, third)
        self.assertTrue(score_3 == 20)


    def test_pair_2(self):
        first_variant = 1
        second_variant = 4
        third_variant = 4
        start_4 = Game()
        score_4 = start_4.throw(first_variant, second_variant, third_variant)
        self.assertTrue(score_4 == 40)


    def test_zero_dice(self):
        first_zero = 2
        second_zero = 0
        third_zero = 6
        self.assertIsNot(first_zero, 0)
        self.assertIsNot(second_zero, 0)
        self.assertIsNot(third_zero, 0)

    def test_for_negative_dice(self):
        first = 2
        second = 4
        third = -4
        start = Game()
        with self.assertRaises(NegativeDice):
            start.throw(first, second, third)

    def test_for_big_dice(self):
        first = 1
        second = 5
        third = 11
        start_game = Game()
        with self.assertRaises(BigDice):
            start_game.throw(first, second, third)


if __name__ == '__main__':
    unittest.main(argv=[''], verbosity=2, exit=False)