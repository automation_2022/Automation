class ZeroDiceError(Exception):
    pass

class NegativeDiceError(Exception):
    pass


class BigDiceError(Exception):
    pass