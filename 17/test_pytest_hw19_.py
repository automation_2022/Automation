import pytest
from game import Game
from exception_for_hw19 import ZeroDiceError, NegativeDiceError, BigDiceError


@pytest.fixture
def check_exception():
    def checker(function, exception: Exception, *args):
        try:
            function(*args)
        except exception:
            return True
        except Exception:
            return False
        return False
    return checker


@pytest.fixture
def game_provider():
    return Game()


@pytest.mark.parametrize("dice_1, dice_2, dice_3, expected_scores",
                         [(1, 2, 3, 6),
                         (2, 2, 2, 200),
                         (3, 1, 3, 30),
                         (1, 5, 5, 50)])
def test_dice_game(game_provider, dice_1: int, dice_2: int, dice_3: int, expected_scores: int):
    score = game_provider.throw(dice_1, dice_2, dice_3)
    assert score == expected_scores, \
    f"Incorrect dice has been returned: {score}. \
    Should be {expected_scores}"


@pytest.mark.parametrize("dice_4, dice_5, dice_6, exception",
                         [(1, 0, 5, ZeroDiceError),
                         (1, 5, -5, NegativeDiceError),
                         (1, 5, 10, BigDiceError)])
def test_negative_scenarios(
    check_exception,
    dice_4,
    dice_5,
    dice_6,
    exception: Exception):
    assert check_exception(dice_4, exception, dice_5, dice_6), \
f"Exception {exception.__name__} hasn't been raised"


