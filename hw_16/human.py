import datetime


class Human:
    def __init__(self,
                 name: str,
                 surname: str,
                 position: str,
                 age=datetime.date.today()):
        self.name = name
        self.surname = surname
        self.age = age
        self.position = position

    @property
    def age_determination(self):
        return (datetime.date.today() - self.age).days // 365


