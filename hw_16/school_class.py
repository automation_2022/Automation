from teacher import Teacher
from pupil import Pupil


class SchoolClass:
    count = 0
    def __init__(self,
                 classes: list,
                 pupils: [Pupil] = None,
                 homeroom_teacher: Teacher = str):
        if pupils is None:
            pupils = []
            classes = []
        self.__pupils = pupils
        self.__homeroom_teacher = homeroom_teacher
        self.classes = classes
        SchoolClass.count += 1

    @classmethod
    def change_class(cls, class_name):
        cls.classes = class_name

    @property
    def home_teacher(self) -> Teacher:
        return self.__homeroom_teacher

    @home_teacher.setter
    def home_teacher(self, new_teacher):
        self.__homeroom_teacher = new_teacher

    @property
    def student(self) -> [Pupil]:
        return self.__pupils

    @student.setter
    def student(self, new_student):
        self.__pupils = new_student

    def get_pupils(self) -> [Pupil]:
        return self.__pupils

    def add_pupils(self, new_pupil):
        self.__pupils.append(new_pupil)

    def remove_pupils(self, removed_pupil):
        del removed_pupil

    def displaycount(self):
        return SchoolClass.count

    def get_classes(self):
        classes = ['A', 'B', 'C', 'D', 'E', 'F']
        for i in range(1, 12):
            print(i, classes)

    def create_new_class(self, new_class):
        self.classes.append(new_class)
        return self.classes, self.__homeroom_teacher

    def remove_new_class(self, removed_class):
        if SchoolClass.get_pupils:
            self.classes.remove(removed_class)
        else:
            print('Can not remove a class')







