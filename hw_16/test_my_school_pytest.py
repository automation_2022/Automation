# Task 1.
# Написати тести на PyTest на класи по приватній школі, що перевіряють
# Для кожного персоналу та учня: після створення імʼя, прізвище та вік відображають реальність
# Після створення класу у ньому є коректна кількість учнів
# При додаванні чи відніманні учня кількість учнів теж коректна

import pytest
from datetime import date
from teacher import Teacher
from director import Director
from cleaner import Cleaner
from head_teacher import HeadTeacher
from pupil import Pupil
from school_class import SchoolClass


def test_director_name():
    director = Director('Mykhailo', 'Bondarenko', 'director',  1000, 0, 4000, 20000, 45)
    assert director.name == 'Mykhailo', \
    f"Incorrect director's name returned: {director.surname}. \
    Should be {director.name}"


def test_director_surname():
    director = Director('Mykhailo', 'Bondarenko', 'director',  1000, 0, 4000, 20000, 45)
    assert director.surname == 'Bondarenko', \
    f"Incorrect director's surname returned: {director.name}. \
    Should be {director.surname}"


def test_director_age():
    director = Director('Mykhailo', 'Bondarenko', 'director',  1000, 0, 4000, 20000, 45)
    assert director.age == 45, \
    f"Incorrect director's age returned: {director.benefit}. \
    Should be {director.age}"


def test_teacher_name():
    teacher = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, 34)
    assert teacher.name == 'Olena', \
    f"Incorrect teacher's name returned: {teacher.surname}. \
    Should be {teacher.name}"


def test_teacher_surname():
    teacher = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, 34)
    assert teacher.surname == 'Sydorenko', \
    f"Incorrect teacher's surname returned: {teacher.name}. \
    Should be {teacher.surname}"


def test_teacher_age():
    teacher = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, 34)
    assert teacher.age == 34, \
    f"Incorrect teacher's age returned: {teacher.benefit}. \
    Should be {teacher.age}"


def test_head_teacher_name():
    head_teacher = HeadTeacher('Mariia', 'Kropivnicka', 'head teacher',  1000, 0, 4000, 15000, 44)
    assert head_teacher.name == 'Mariia', \
    f"Incorrect head teacher's name returned: {head_teacher.surname}. \
    Should be {head_teacher.name}"


def test_head_teacher_surname():
    head_teacher = HeadTeacher('Mariia', 'Kropivnicka', 'head teacher',  1000, 0, 4000, 15000, date(1978, 8, 8))
    assert head_teacher.surname == 'Kropivnicka', \
    f"Incorrect head teacher's surname returned: {head_teacher.name}. \
    Should be {head_teacher.surname}"


def test_head_teacher_age():
    head_teacher = HeadTeacher('Mariia', 'Kropivnicka', 'head teacher',  1000, 0, 4000, 15000, 44)
    assert head_teacher.age == 44, \
    f"Incorrect head teacher's age returned: {head_teacher.benefit}. \
    Should be {head_teacher.age}"


def test_cleaner_name():
    cleaner = Cleaner('Iryna', 'Rudenko', 'cleaner', 1000, 0, 500, 3000, 54)
    assert cleaner.name == 'Iryna', \
    f"Incorrect cleaner's name returned: {cleaner.surname}. \
    Should be {cleaner.name}"


def test_cleaner_surname():
    cleaner = Cleaner('Iryna', 'Rudenko', 'cleaner', 1000, 0, 500, 3000, 54)
    assert cleaner.surname == 'Rudenko', \
    f"Incorrect cleaner's surname returned: {cleaner.name}. \
    Should be {cleaner.surname}"


def test_cleaner_age():
    cleaner = Cleaner('Iryna', 'Rudenko', 'cleaner', 1000, 0, 500, 3000, 54)
    assert cleaner.age == 54, \
    f"Incorrect cleaner's age returned: {cleaner.benefit}. \
    Should be {cleaner.age}"


def test_pupil1_name():
    pupil1 = Pupil('Mykola', 'Shevchenko', 'pupil', 0, 1, 1, 0, date(2001, 2, 2))
    pupil2 = Pupil('Tanya', 'Ivanenko', 'pupil', 2, 5, 5, 0, date(2001, 5, 6))
    assert pupil1.name == 'Mykola', \
    f"Incorrect pupil's name returned: {pupil2.name}. \
    Should be {pupil1.name}"


def test_pupil2_name():
    pupil1 = Pupil('Mykola', 'Shevchenko', 'pupil', 0, 1, 1, 0, date(2001, 2, 2))
    pupil2 = Pupil('Tanya', 'Ivanenko', 'pupil', 2, 5, 5, 0, date(2001, 5, 6))
    assert pupil2.name == 'Tanya', \
    f"Incorrect pupil's name returned: {pupil1.name}. \
    Should be {pupil2.name}"


def test_pupil3_name():
    pupil3 = Pupil('Ivan', 'Mykolenko', 'pupil', 3, 6, 6, 0, date(2001, 5, 6))
    pupil4 = Pupil('Myroslav', 'Fedorenko', 'pupil', 10, 1, 1, 0, date(2002, 5, 9))
    assert pupil3.name == 'Ivan', \
    f"Incorrect pupil's name returned: {pupil4.name}. \
    Should be {pupil3.name}"


def test_pupil4_name():
    pupil3 = Pupil('Ivan', 'Mykolenko', 'pupil', 3, 6, 6, 0, date(2001, 5, 6))
    pupil4 = Pupil('Myroslav', 'Fedorenko', 'pupil', 10, 1, 1, 0, date(2002, 5, 9))
    assert pupil4.name == 'Myroslav', \
    f"Incorrect pupil's name returned: {pupil3.name}. \
    Should be {pupil4.name}"


def test_pupil1_surname():
    pupil1 = Pupil('Mykola', 'Shevchenko', 'pupil', 0, 1, 1, 0, date(2001, 2, 2))
    pupil2 = Pupil('Tanya', 'Ivanenko', 'pupil', 2, 5, 5, 0, date(2001, 5, 6))
    assert pupil1.surname == 'Shevchenko', \
    f"Incorrect pupil's name returned: {pupil2.surname}. \
    Should be {pupil1.surname}"


def test_pupil2_surname():
    pupil1 = Pupil('Mykola', 'Shevchenko', 'pupil', 0, 1, 1, 0, date(2001, 2, 2))
    pupil2 = Pupil('Tanya', 'Ivanenko', 'pupil', 2, 5, 5, 0, date(2001, 5, 6))
    assert pupil2.surname == 'Ivanenko', \
    f"Incorrect pupil's name returned: {pupil1.surname}. \
    Should be {pupil2.surname}"


def test_pupil3_surname():
    pupil3 = Pupil('Ivan', 'Mykolenko', 'pupil', 3, 6, 6, 0, date(2001, 5, 6))
    pupil4 = Pupil('Myroslav', 'Fedorenko', 'pupil', 10, 1, 1, 0, date(2002, 5, 9))
    assert pupil3.surname == 'Mykolenko', \
    f"Incorrect pupil's name returned: {pupil4.surname}. \
    Should be {pupil3.surname}"


def test_pupil4_surname():
    pupil3 = Pupil('Ivan', 'Mykolenko', 'pupil', 3, 6, 6, 0, date(2001, 5, 6))
    pupil4 = Pupil('Myroslav', 'Fedorenko', 'pupil', 10, 1, 1, 0, date(2002, 5, 9))
    assert pupil4.surname == 'Fedorenko', \
    f"Incorrect pupil's name returned: {pupil3.surname}. \
    Should be {pupil4.surname}"


def test_pupil1_age():
    pupil1 = Pupil('Mykola', 'Shevchenko', 'pupil', 0, 1, 1, 0, 11)
    pupil2 = Pupil('Tanya', 'Ivanenko', 'pupil', 2, 5, 5, 0, 10)
    assert pupil1.age == 11, \
    f"Incorrect pupil's name returned: {pupil2.age}. \
    Should be {pupil1.age}"


def test_pupil2_age():
    pupil1 = Pupil('Mykola', 'Shevchenko', 'pupil', 0, 1, 1, 0, 11)
    pupil2 = Pupil('Tanya', 'Ivanenko', 'pupil', 2, 5, 5, 0, 10)
    assert pupil2.age == 10, \
    f"Incorrect pupil's name returned: {pupil1.age}. \
    Should be {pupil2.age}"


def test_pupil3_age():
    pupil3 = Pupil('Ivan', 'Mykolenko', 'pupil', 3, 6, 6, 0, 9)
    pupil4 = Pupil('Myroslav', 'Fedorenko', 'pupil', 10, 1, 1, 0, 8)
    assert pupil3.age == 9, \
    f"Incorrect pupil's name returned: {pupil4.age}. \
    Should be {pupil3.age}"


def test_pupil4_age():
    pupil3 = Pupil('Ivan', 'Mykolenko', 'pupil', 3, 6, 6, 0, 9)
    pupil4 = Pupil('Myroslav', 'Fedorenko', 'pupil', 10, 1, 1, 0, 8)
    assert pupil4.age == 8, \
    f"Incorrect pupil's name returned: {pupil3.age}. \
    Should be {pupil4.age}"


def test_get_pupils():
    teacher = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, 44)
    class_room_1 = SchoolClass(['A'], [Pupil], teacher)
    assert class_room_1.count == class_room_1.displaycount()


def test_add_pupils():
    teacher = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, 43)
    pupil4 = Pupil('Myroslav', 'Fedorenko', 'pupil', 10, 1, 1, 0, 11)
    class_room_2 = SchoolClass(['B'], [Pupil], teacher)
    class_room_2.add_pupils(pupil4)
    assert class_room_2.count == class_room_2.displaycount()


def test_remove_pupils():
    teacher = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, 23)
    pupil3 = Pupil('Ivan', 'Mykolenko', 'pupil', 3, 6, 6, 0, 11)
    class_room_2 = SchoolClass(['B'], [Pupil], teacher)
    class_room_2.remove_pupils(pupil3)
    assert class_room_2.count == class_room_2.displaycount()



