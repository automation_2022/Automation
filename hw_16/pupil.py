from personnel import Personnel
import datetime


class Pupil(Personnel):
    _counter = 0

    def __init__(self,
                 name: str,
                 surname: str,
                 position: str,
                 expense: int,
                 marks: int,
                 benefit: int,
                 salary=0,
                 age=datetime.date.today()):
        super().__init__(name, surname, position, expense,  marks, benefit, salary, age)
        self.marks = marks
        Pupil._counter += 1

    @property
    def age_determination(self):
        return (datetime.date.today() - self.age).days // 365

    def calculate_average(self, total):
        scores = []
        for i in range(1, 6):
            score = int(input('Enter score {0}: '.format(i)))
            scores.append(score)
        total = sum(scores)
        school_average_mark = total / 5
        print('Average grade is: ' + str(school_average_mark))


    def reward(self):
        self.benefit += self.marks
        return self.marks




