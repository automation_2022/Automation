from datetime import date
from teacher import Teacher
from director import Director
from cleaner import Cleaner
from head_teacher import HeadTeacher
from pupil import Pupil
from school import School
from school_class import SchoolClass
from personnel import Personnel

if __name__ == '__main__':
    pupil1 = Pupil('Mykola', 'Shevchenko', 'pupil', 0, 1, 1, 0, date(2001, 2, 2))
    pupil2 = Pupil('Tanya', 'Ivanenko', 'pupil', 2, 5, 5, 0, date(2001, 5, 6))
    pupil3 = Pupil('Ivan', 'Mykolenko', 'pupil', 3, 6, 6, 0, date(2001, 5, 6))
    pupil4 = Pupil('Myroslav', 'Fedorenko', 'pupil', 10, 1, 1, 0, date(2002, 5, 9))
    pupil5 = Pupil('Oleksii', 'Rozumenko', 'pupil', 11, 2, 2, 0, date(2001, 8, 8))
    pupil1.calculate_average(5)
    pupil2.reward()

    teacher1 = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, date(1988, 7, 8))
    teacher2 = Teacher('Svitlana', 'Ponomarenko', 'teacher', 1000, 0, 4000, 6000, date(1988, 7, 8))

    class_room_1 = SchoolClass(['A'], pupil2, teacher1)
    class_room_2 = SchoolClass(['B'], pupil4, teacher2)
    class_room_1.get_pupils()
    class_room_1.get_classes()

    director1 = Director('Mykhailo', 'Bondarenko', 'director',  1000, 0, 4000, 20000, date(1977, 3, 23))

    head_teacher1 = HeadTeacher('Mariia', 'Kropivnicka', 'homeroom teacher',  1000, 0, 4000, 15000, date(1978, 8, 8))

    cleaner1 = Cleaner('Iryna', 'Rudenko', 'cleaner', 1000, 0, 500, 3000, date(1968, 9, 8))

    school1 = School('Sun', director1, head_teacher1, teacher2, cleaner1, class_room_2)

    school1.get_teachers()

    worker1 = Personnel('Kateryna', 'Marchenko', 'school worker', 2000, 5, 1000, 6000, date(1997, 2, 2))
    worker1.amount()
    worker1.remove_salary()
    worker1.calculate_average(5)





