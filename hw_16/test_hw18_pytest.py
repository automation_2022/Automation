# Task 1.
# Вживаючи PyTest, напишіть тести для проєкту Приватна Школа . Обов'язково протестайте наступні речі:
# Учня можна додати до класу. Результати підрахунку маржі теж зміняться
# Учня можна видалити з класу. Результати підрахунку маржі теж зміняться
# Правильність підрахунку маржі
# Додавання одного і того самого учня - не вдасться додати одного і того ж учня двічі


import pytest
from school_class import SchoolClass
from teacher import Teacher
from pupil import Pupil
from except_hw16 import CanNotAddOnePupilTwice


class Config:
    student_amount = None
    add_student = None
    remove_student = None


@pytest.fixture(scope="function")
def check_exception():
    def checker(function, exception: Exception, *args):
        try:
            print(f"Checking for exception {exception} \ for function {function.__name__}")
            function(*args)
        except exception:
            return True
        except Exception:
            return False
        return False
    return checker


@pytest.fixture(scope="session", autouse=True)
def student_amount(request):
    print(f"Counting students {SchoolClass}")
    Config.student_amount = SchoolClass

    def teardown():
        print("Deleting class")
        del Config.student_amount
    request.addfinalizer(teardown)


@pytest.fixture(scope="function")
def add_student():
    print(f"Adding a student to {SchoolClass}")
    Config.add_student = SchoolClass


@pytest.fixture(scope="function")
def remove_student():
    print(f"Removing a student from {SchoolClass}")
    Config.remove_student = SchoolClass


def test_student_count():
    teacher_2 = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, 44)
    class_room_1 = Config.student_amount(['A'], [Pupil], teacher_2)
    display_count = class_room_1.displaycount()
    assert class_room_1.count == display_count


def test_student_add(add_student):
    teacher = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, 43)
    pupil = Pupil('Myroslav', 'Fedorenko', 'pupil', 10, 1, 1, 0, 11)
    class_room_2 = Config.add_student(['B'], [Pupil], teacher)
    class_room_2.add_pupils(pupil)
    display_count_2 = class_room_2.displaycount()
    assert class_room_2.count == display_count_2


def test_student_remove(remove_student):
    teacher_3 = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, 43)
    pupil_3 = Pupil('Myroslav', 'Fedorenko', 'pupil', 10, 1, 1, 0, 11)
    class_room_3 = Config.remove_student(['B'], [Pupil], teacher_3)
    class_room_3.remove_pupils(pupil_3)
    display_count_3 = class_room_3.displaycount()
    assert class_room_3.count == display_count_3


def test_one_student(check_exception):
    teacher = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, 43)
    pupil = Pupil('Myroslav', 'Fedorenko', 'pupil', 10, 1, 1, 0, 11)
    class_room_2 = SchoolClass(['B'], [Pupil], teacher)
    add = class_room_2.add_pupils(pupil)
    assert check_exception(add, CanNotAddOnePupilTwice), f"Exception CanNotAddOnePupilTwice hasn't been raised when student is created twice"




