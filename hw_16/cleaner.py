from personnel import Personnel
import datetime


class Cleaner(Personnel):
    def __init__(self,
                 name: str,
                 surname: str,
                 position: str,
                 expense: int,
                 marks: int,
                 benefit: int,
                 salary=3000,
                 age=datetime.date.today()):
        super().__init__(name, surname, position, expense, marks, benefit, salary, age)

    @property
    def age_determination(self):
        return (datetime.date.today() - self.age).days // 365
