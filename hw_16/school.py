from director import Director
from head_teacher import HeadTeacher
from teacher import Teacher
from cleaner import Cleaner
from school_class import SchoolClass


class School:

    def __init__(self,
                 name: str,
                 director: Director,
                 head_teacher: HeadTeacher,
                 teacher: [Teacher],
                 cleaner: Cleaner,
                 classes: SchoolClass):
        self.name = name
        self.__director = director
        self.__head_teacher = head_teacher
        self.__teacher = teacher
        self.__cleaner = cleaner
        self.classes = classes

    @property
    def director(self) -> Director:
        return self.__director

    @director.setter
    def director(self, new_director):
        self.__director = new_director

    @property
    def head_teacher(self) -> HeadTeacher:
        return self.__head_teacher

    @head_teacher.setter
    def head_teacher(self, new_head_teacher):
        self.__head_teacher = new_head_teacher

    @property
    def teacher(self) -> Teacher:
        return self.__teacher

    @teacher.setter
    def teacher(self, new_teacher):
        self.__teacher = new_teacher

    @property
    def cleaner(self) -> Cleaner:
        return self.__cleaner

    @cleaner.setter
    def cleaner(self, new_cleaner):
        self.__cleaner = new_cleaner

    def get_teachers(self) -> [Teacher]:
        return self.__teacher

    def add_teachers(self, new_teacher):
        self.__teacher.append(new_teacher)

    def remove_teachers(self, removed_teacher):
        self.__teacher.remove(removed_teacher)