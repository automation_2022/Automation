from human import Human
import datetime


class Personnel(Human):
    _counter = 0

    def __init__(self,
                 name: str,
                 surname: str,
                 position: str,
                 expense: int,
                 marks: int,
                 benefit: int,
                 salary: int,
                 age=datetime.date.today()):
        super().__init__(name, surname, position, age)
        self.salary = salary
        self.benefit = benefit
        self.expense = expense
        self.marks = marks
        Personnel._counter += 1

    @property
    def age_determination(self):
        return (datetime.date.today() - self.age).days // 365

    def amount(self):
        payment = int(input())
        for pay in range(payment):
            self.expense += self.salary
            self.expense += self.benefit
        return self.expense

    def remove_salary(self):
        self.expense -= self.salary

    def calculate_average(self, total):
        scores = []
        for i in range(1, 6):
            score = int(input('Enter score {0}: '.format(i)))
            scores.append(score)
        total = sum(scores)
        school_average_mark = total / 5
        print('Average grade is: ' + str(school_average_mark))
        return total / 5














