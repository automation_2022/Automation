from personnel import Personnel
import datetime
from dataclasses import dataclass
from type_gender import GenderDefinition

@dataclass
class Director(Personnel):
    name: str
    surname: str
    position: str
    expense: int
    marks: int
    benefit: int
    salary = 20000
    age = datetime.date.today()


    @property
    def age_determination(self):
        return (datetime.date.today() - self.age).days // 365

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def claim_director_gender(self):
        print(f"{Director.position} is {GenderDefinition.MALE}")

