from human import Human
from datetime import date
from dataclasses import dataclass, field
from type_gender import GenderDefinition

@dataclass(order=True)
class Personnel(Human):
    name: str = field(compare=False)
    surname: str = field(compare=False)
    position: str = field(compare=False)
    expense: int = field(compare=False)
    marks: int = field(compare=False)
    benefit: int = field(compare=False)
    salary: int = field(compare=False)
    age: date = field(compare=False)
    sorting: int = field(init=False, repr=False, compare=True)

    def __post_init__(self):
        if not isinstance(self.salary, int):
            raise TypeError(
                f"Incorrect data type of 'salary' parameter: {type(self.salary)}. Should be: int!")

        self.sort_index = self.amount()

    @property
    def age_determination(self):
        return (date.today() - self.age).days // 365

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def amount(self):
        payment = int(input())
        for pay in range(payment):
            self.expense += self.salary
            self.expense += self.benefit
        return self.expense

    def remove_salary(self):
        self.expense -= self.salary

    def calculate_average(self, total):
        scores = []
        for i in range(1, 6):
            score = int(input('Enter score {0}: '.format(i)))
            scores.append(score)
        total = sum(scores)
        school_average_mark = total / 5
        print('Average grade is: ' + str(school_average_mark))
        return total / 5

    def claim_personnel_gender(self):
        print(f"{Personnel.position} are {GenderDefinition.FEMALE} and {GenderDefinition.MALE}")














