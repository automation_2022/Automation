from personnel import Personnel
import datetime
from dataclasses import dataclass
from type_gender import GenderDefinition

@dataclass
class Teacher(Personnel):
    name: str
    surname: str
    position: str
    expense: int
    marks: int
    benefit: int
    salary = 6000
    age = datetime.date.today()

    @property
    def age_determination(self):
        return (datetime.date.today() - self.age).days // 365

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def claim_teacher_gender(self):
        print(f"{Teacher.position} is {GenderDefinition.FEMALE}")
