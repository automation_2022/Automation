#  Task 1.
# Технічне завдання
# Розробити програму, яка підрахує на основі зарплат вчителів, директора, заучів та прибиральників, скільки дітей
# та з якою оплатою за навчання треба формувати класи на наступний навчальний рік.
# Рівно так само програма має виводити середню оцінку учнів по класу і загалом по школі
# (для спрощення завдання учневі ми приписуємо лише середній бал).
# Деталі
# Кожна людина має:
# Імʼя
# Прізвище
# вік
# посада згідно штатного розпису
# Персонал школи є людьми, які мають:
# Зарплату
# В школі є наступний персонал:
# Директор (зп - 20000 грн в місяць)
# Завуч (зп - 15000 грн в місяць)
# Вчитель (зп - 6000 грн в місяць)
# Учень теж належить до персоналу, але: він платить за навчання...
# І якщо персонал отримує винагороду через отримання зарплати, то учні отримують винагороду через гарні оцінки!
# (врахуйте це. Отимання винагороди - абстрактна дія, яка для певних видів персоналу означає різне).
# Школа містить у собі класів. Кожен клас має список учнів. Учнів можна до класу додавати, видаляти,
# виводити список учнів на консоль. Також клас має учителя - класного керівника, що веде клас.
# Класи можна додавати (додається пустий клас, але з класним керівником), видаляти
# (інші учні мають бути або переведеним в інший клас, або виключеним зі школи).

class School:
    _counter = 0

    def __init__(self,
                 name: str,
                 expense: int,
                 salary: int,
                 marks: int,
                 worker: str):
        self.name = name
        self.expense = expense
        self.worker = worker
        self.salary = salary
        self.marks = marks
        School._counter += 1

    def amount(self):
        payment = int(input())
        for pay in range(payment):
            self.expense += self.salary
            self.expense += self.worker
        return self.expense

    @classmethod
    def change_teacher(cls, changed_teacher):
        cls.changed_teacher = changed_teacher

    def remove_salary(self):
        if School.change_teacher:
            self.expense -= self.salary

    def calculate_school_marks(self):
        mark = []
        total = 0
        for i in range(10):
            mark.insert(i, int(input()))
        for i in range(10):
            total = total + mark[i]
        school_average_mark = total / 10
        print(f'School average mark {school_average_mark}')


if __name__ == '__main__':
    test = School('Sun', 4000, 30000, 7, 'Teacher')
    print(School.change_teacher('New Teacher'))
    print(test.amount())
    print(test.remove_salary())
    print(test.calculate_school_marks())


