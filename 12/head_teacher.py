from personnel import Personnel


class HeadTeacher(Personnel):
    def __init__(self,
                 name: str,
                 surname: str,
                 age: int,
                 position: str,
                 benefit: int,
                 salary=15000):
        super().__init__(name, surname, age, position,  benefit, salary)

