from abc import ABC, abstractmethod


class Human(ABC):
    def __init__(self,
                 name: str,
                 surname: str,
                 age: int,
                 position: str):
        self.name = name
        self.surname = surname
        self.age = age
        self.position = position

    @abstractmethod
    def reward(self):
        pass

    @abstractmethod
    def calculate_marks(self):
        pass




