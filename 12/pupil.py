from personnel import Personnel


class Pupil(Personnel):
    _counter = 0

    def __init__(self,
                 name: str,
                 surname: str,
                 age: int,
                 position: str,
                 benefit: int,
                 marks: int,
                 salary=0):
        super().__init__(name, surname, age, position, benefit, salary)
        self.marks = marks
        Pupil._counter += 1

    def calculate_marks(self):
        mark = []
        total = 0
        for i in range(10):
            mark.insert(i, int(input()))
        for i in range(10):
            total = total + mark[i]
            self.benefit += self.marks
        average_mark = total / 10
        print(f'Class average mark {average_mark}')

    def reward(self):
        self.benefit += self.marks
        return self.marks


if __name__ == '__main__':
    test = Pupil('Mykola', 'Shevchenko', 8, 'pupil', 4, 9, 7)
    print(test.calculate_marks())
    print(test.reward())