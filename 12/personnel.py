from human import Human


class Personnel(Human):
    def __init__(self,
                 name: str,
                 surname: str,
                 age: int,
                 position: str,
                 benefit: int,
                 marks: int,
                 salary: int):
        super().__init__(name, surname, age, position)
        self.salary = salary
        self.benefit = benefit
        self.marks = marks

    def calculate_marks(self):
        mark = []
        total = 0
        for i in range(10):
            mark.insert(i, int(input()))
        for i in range(10):
            total = total + mark[i]
            self.benefit += self.marks
        average_mark = total / 10
        return average_mark

    def reward(self):
        self.benefit += self.marks
        return self.marks















