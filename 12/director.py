from personnel import Personnel


class Director(Personnel):
    def __init__(self,
                 name: str,
                 surname: str,
                 age: int,
                 position: str,
                 benefit: int,
                 salary=20000):
        super().__init__(name, surname, age, position, salary, benefit)


