from personnel import Personnel
import datetime
from dataclasses import dataclass
from type_gender import GenderDefinition

@dataclass
class HeadTeacher(Personnel):
    name: str
    surname: str
    position: str
    expense: int
    marks: int
    benefit: int
    salary = 15000
    age = datetime.date.today()

    @property
    def age_determination(self):
        return (datetime.date.today() - self.age).days // 365

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def claim_head_teacher_gender(self):
        print(f"{HeadTeacher.position} is {GenderDefinition.FEMALE}")


