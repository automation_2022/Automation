from enum import Enum


class GenderDefinition(Enum):
    FEMALE = "Female"
    MALE = "Male"

    def __str__(self):
        return self.value

    def __repr__(self):
        return self.value

    def custom_asdict_factory(self, data):
        def convert_value(obj):
            if isinstance(obj, Enum):
                return obj.value
            return obj

        return dict((k, convert_value(v)) for k, v in data)


print(f"Director is {GenderDefinition.MALE}")

print(f"Teacher is {GenderDefinition.FEMALE}")

print(f"Head teacher is {GenderDefinition.FEMALE}")

print(f"There are {GenderDefinition.FEMALE} and {GenderDefinition.MALE} workers")

print(f"There are {GenderDefinition.FEMALE} and {GenderDefinition.MALE} pupils")

print(f"There are {GenderDefinition.FEMALE} and {GenderDefinition.MALE} humans")