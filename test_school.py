import unittest
from datetime import date
from pupil import Pupil
from school import School
from director import Director
from head_teacher import HeadTeacher
from type_gender import Cleaner
from teacher import Teacher
from school_class import SchoolClass


class SchoolTest(unittest.TestCase):

    def setUp(self) -> None:
        director = Director('Mykhailo', 'Bondarenko', 'director',  1000, 0, 4000, 20000, date(1977, 3, 23))
        pupil = Pupil('Mykola', 'Shevchenko', 'pupil', 0, 1, 1, 0, date(2001, 2, 2))
        head_teacher = HeadTeacher('Mariia', 'Kropivnicka', 'homeroom teacher',  1000, 0, 4000, 15000, date(1978, 8, 8))
        cleaner = Cleaner('Iryna', 'Rudenko', 'cleaner', 1000, 0, 500, 3000, date(1968, 9, 8))
        teacher = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, date(1988, 7, 8))
        class_room = SchoolClass(['A'], pupil, teacher)
        self.school = School('Sun', director, head_teacher, teacher, cleaner, class_room)

    def test_director(self):
        self.assertTrue(self.school.director.name == 'Mykhailo')
        self.assertTrue(self.school.director.surname == 'Bondarenko')
        self.assertTrue(self.school.director.salary == 20000)

    def test_head_teacher(self):
        self.assertTrue(self.school.head_teacher.name == 'Mariia')
        self.assertTrue(self.school.head_teacher.surname == 'Kropivnicka')
        self.assertTrue(self.school.head_teacher.salary == 15000)

    def test_cleaner(self):
        self.assertTrue(self.school.cleaner.name == 'Iryna')
        self.assertTrue(self.school.cleaner.surname == 'Rudenko')
        self.assertTrue(self.school.cleaner.salary == 3000)

    def test_teacher(self):
        self.assertTrue(self.school.teacher.name == 'Olena')
        self.assertTrue(self.school.teacher.surname == 'Sydorenko')
        self.assertTrue(self.school.teacher.salary == 6000)

    def test_school_name(self):
        self.assertEqual(self.school.name, 'Sun')

    def test_class_room(self):
        pupil = Pupil('Mykola', 'Shevchenko', 'pupil', 0, 1, 1, 0, date(2001, 2, 2))
        teacher = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, date(1988, 7, 8))
        class_room = SchoolClass(['A'], pupil, teacher)
        self.assertIsInstance(class_room, SchoolClass)


if __name__ == '__main__':
    unittest.main()