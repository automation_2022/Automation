from abc import ABC, abstractmethod
from bonus import Discount


def ask_drink():
    choices_coffee = {1: 'latte', 2: 'cappuccino', 3: 'americano'}
    possible_values_coffee = ", ".join(f"{value} - {name}" for value, name in sorted(choices_coffee.items()))
    while True:
        answer_1 = input(f"What do you want to buy? {possible_values_coffee}:\n")
        try:
            value_1 = int(answer_1)
            if value_1 in choices_coffee:
                return value_1
            print(f'This answer is not valid: {answer_1}')
        except ValueError:
            print('This answer is not valid')


def ask_seasoning():
    choices_add = {4: 'syrup', 5: 'cinnamon', 6: 'sugar', 0: 'no seasoning'}
    possible_values_add = ", ".join(f"{value} - {name}" for value, name in sorted(choices_add.items()))
    while True:
        answer_2 = input(f"Do you want to add seasoning? {possible_values_add}:\n")
        try:
            value_2 = int(answer_2)
            if value_2 in choices_add:
                return value_2
            print(f'This answer is not valid: {answer_2}')
        except ValueError:
            print('This answer is not valid')


def coffee_machine(cls, description, price):
    old_cost = cls.cost
    old_description = cls.get_description

    def cost(self):
        new_cost = old_cost(self) + price
        return new_cost

    def get_description(self):
        new_description = old_description(self) + description
        return new_description

    cls.cost = cost
    cls.get_description = get_description
    return cls


def add_sugar(cls):
    def wrapper():
        return coffee_machine(cls, 'Sugar added.', 5)
    return wrapper()


def add_cinnamon(cls):
    def wrapper():
        return coffee_machine(cls, 'Cinnamon added.', 6)
    return wrapper()


def add_syrup(cls):
    def wrapper():
        return coffee_machine(cls, 'Syrup added.', 10)
    return wrapper()


class Coffee(ABC):
    def __init__(self,
                 brand: str,
                 coffee_type: str,
                 base_price: int):
        self._brand = brand
        self._coffee_type = coffee_type
        self._base_price = base_price

    @property
    def get_bonus(self):
        if self._base_price <= 0:
            return Discount.BONUS_0
        elif 50 < self._base_price < 99:
            return Discount.BONUS_50
        elif 100 < self._base_price < 249:
            return Discount.BONUS_100
        elif 250 < self._base_price < 299:
            return Discount.BONUS_250
        elif self._base_price >= 300:
            return Discount.BONUS_300

    @abstractmethod
    def cost(self):
        pass

    @abstractmethod
    def get_description(self):
        pass














