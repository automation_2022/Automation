from except_file import InsufficientFundsError


class Currency:
    def __init__(self,
                 money_received: int,
                 profit: int):
        self.profit = profit
        self.money_received = money_received
        if self.money_received <= 0:
            raise InsufficientFundsError(f'Insufficient funds: {str(self.money_received)}')

    def process_money(self):
        print("Please, insert money.")
        total = int(input('How much?'))
        return total

    def make_payment(self, payment):

        self.process_money()
        if self.money_received >= payment:
            self.profit += payment
            return True
