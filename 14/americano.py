from coffee import Coffee, add_sugar
from bonus import Discount


@add_sugar
class Americano(Coffee):
    def __init__(self):
        super().__init__('Costa Coffee', 'Arabic', 101)

    def get_description(self):
        return f'{self._brand}, {self._coffee_type} base price is {self._base_price} hryvnias. '

    def cost(self):
        return self._base_price

    def get_bonus(self):
        return Discount.BONUS_100