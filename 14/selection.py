from coffee import ask_drink
from latte import Latte
from capuccino import Capuccino
from americano import Americano

value_1 = ask_drink()


def selection_drink():
    if value_1 == 1:
        cup_of_coffee3 = Latte()
        print('Your selection is latte:', cup_of_coffee3.get_description())
        print('As seasoning was added, new coffee price:', cup_of_coffee3.cost())
        print('You got discount for next visit: ', cup_of_coffee3.get_bonus(), '%')
    elif value_1 == 2:
        cup_of_coffee2 = Capuccino()
        print('Your selection is cappuccino:',  cup_of_coffee2.get_description())
        print('As seasoning was added, new coffee price:', cup_of_coffee2.cost())
        print('You got discount for next visit: ', cup_of_coffee2.get_bonus(), '%')
    elif value_1 == 3:
        cup_of_coffee1 = Americano()
        print('Your selection is americano:', cup_of_coffee1.get_description())
        print('As seasoning was added, new coffee price:', cup_of_coffee1.cost())
        print('You got discount for next visit: ', cup_of_coffee1.get_bonus(), '%')
    elif value_1 == 0:
        print('Thank you')
    return value_1




