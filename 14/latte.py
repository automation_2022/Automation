from coffee import Coffee, add_syrup
from bonus import Discount


@add_syrup
class Latte(Coffee):
    def __init__(self):
        super().__init__('Starbucks', 'Arabic', 70)

    def get_description(self):
        return f'{self._brand}, {self._coffee_type} base price is {self._base_price} hryvnias. '

    def cost(self):
        return self._base_price

    def get_bonus(self):
        return Discount.BONUS_50
