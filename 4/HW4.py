#  Task 1. Дана довільна строка.
#  Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.

string_1 = input()


def is_vowel(char):
    return char.lower() in 'aeiouаеіиоуаыиаоуэ'


def is_consecutive(word):
    counter = 0
    for letter in word:
        if is_vowel(letter):
            counter += 1
            if counter == 2:
                return True
        else:
            counter = 0

    return False


def word_count(str):
    quantity = dict()
    phrases = str.split()
    for phrase in phrases:
        if is_vowel(phrase) in quantity and is_consecutive(phrase) in quantity:
            quantity[phrase] += 1
    else:
        quantity[phrase] = 1

    return quantity


print("Кількість слів, які містять дві голосні літери підряд:" + str(word_count(string_1)))


#  Task 2. Є два довільних числа які відповідають за мінімальну і максимальну ціну.
#  Є Dict з назвами магазинів і цінами:
#  { "cilpio": 47.999, "a_studio" 42.999, "momo": 49.999, "main-service": 37.245,
#  "buy.ua": 38.324, "my-store": 37.166, "the_partner": 38.988, "sto": 37.720, "rozetka": 38.003}.
#  Напишіть код, який знайде і виведе на екран назви магазинів,
#  ціни яких попадають в діапазон між мінімальною і максимальною ціною.


my_dict = {
    "cilpio": 47.999,
    "a_studio": 42.999,
    "momo": 49.999,
    "main-service": 37.245,
    "buy.ua": 38.324,
    "my-store": 37.166,
    "the_partner": 38.988,
    "sto": 37.720,
    "rozetka": 38.003,
}


for item in my_dict.items():
    minn = min(my_dict.values())
    maxx = max(my_dict.values())
    key, value = item
    if minn < value < maxx:
        print("Shop between lower and upper limits: ", key)







