from datetime import date
from teacher import Teacher
from director import Director
from head_teacher import HeadTeacher
from pupil import Pupil
from personnel import Personnel
from human import Human

if __name__ == '__main__':
    school_pupils = [
        Pupil('Mykola', 'Shevchenko', 'pupil', 0, 1, 1, 0, date(2001, 2, 2)),
        Pupil('Tanya', 'Ivanenko', 'pupil', 2, 5, 5, 0, date(2001, 5, 6)),
        Pupil('Ivan', 'Mykolenko', 'pupil', 3, 6, 6, 0, date(2001, 5, 6)),
        Pupil('Myroslav', 'Fedorenko', 'pupil', 10, 1, 1, 0, date(2002, 5, 9)),
        Pupil('Oleksii', 'Rozumenko', 'pupil', 11, 2, 2, 0, date(2001, 8, 8))
    ]
    sorted_pupils = sorted(school_pupils, key=lambda p: p.age)

    salaries = [
        Personnel('Mykhailo', 'Bondarenko', 'director', 1000, 0, 4000, 20000, date(1977, 3, 23)),
        Personnel('Mariia', 'Kropivnicka', 'head teacher', 1000, 0, 4000, 15000, date(1978, 8, 8)),
        Personnel('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, date(1988, 7, 8))
    ]

    sorted_salaries = sorted(salaries, key=lambda p: p.salary)

    teacher1 = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, date(1988, 7, 8))
    print(f"{teacher1.name} {teacher1.surname} is {teacher1.position}")
    same_teacher1 = Teacher('Olena', 'Sydorenko', 'teacher', 1000, 0, 1000, 6000, date(1988, 7, 8))
    print(teacher1 == same_teacher1)

    director1 = Director('Mykhailo', 'Bondarenko', 'director', 1000, 0, 4000, 20000, date(1977, 3, 23))
    print(f"{director1.name} {director1.surname} is {director1.position}")
    same_director1 = Director('Mykhailo', 'Bondarenko', 'director', 1000, 0, 4000, 20000, date(1977, 3, 23))
    print(director1 == same_director1)

    head_teacher1 = HeadTeacher('Mariia', 'Kropivnicka', 'head teacher', 1000, 0, 4000, 15000, date(1978, 8, 8))
    print(f"{head_teacher1.name} {head_teacher1.surname} is {head_teacher1.position}")
    same_head_teacher1 = HeadTeacher('Mariia', 'Kropivnicka', 'head teacher', 1000, 0, 4000, 15000, date(1978, 8, 8))
    print(same_head_teacher1 == head_teacher1)

    pupil1 = Pupil('Oleksii', 'Rozumenko', 'pupil', 11, 2, 2, 0, date(2001, 8, 8))
    personnel1 = Personnel('Andrii', 'Ivanov', 'teacher', 11, 2, 2, 0, date(1992, 5, 8))
    human1 = Human('Andrii', 'Ivanov', 'human')
    personnel1.claim_personnel_gender()
    human1.claim_humans_gender()
    teacher1.claim_teacher_gender()
    head_teacher1.claim_head_teacher_gender()
    director1.claim_director_gender()
    pupil1.claim_pupils_gender()

