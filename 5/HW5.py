#  Task 1. Напишіть функцію, що приймає один аргумант.
#  Функція має вивести на ектан тип цього аргумента (для визначення типу використайте type)

def one_argument(argument):
    print('Argument data type:', type(argument))
    return argument


argument_type = one_argument(1)
print(argument_type)


#  Task 2.
#  Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float.
#  Якщо перетворити не вдається функція має повернути 0 (флоатовий нуль).


def float_number(number):
    try:
        return float(number)
    except:
        return 0.0


result_2 = float_number(1)

#  Task 3.
#  Напишіть функцію, що приймає два аргументи. Функція повинна
#  якщо аргументи відносяться до числових типів - повернути різницю цих аргументів,
#  якщо обидва аргументи це строки - обʼєднати в одну строку та повернути
#  якщо перший строка, а другий ні - повернути dict де ключ це перший аргумент, а значення - другий
#  у будь-якому іншому випадку повернути кортеж з цих аргументів


def my_function(my_arg1, my_arg2):
    value_1, value_2 = input(), input()
    my_dict = {
        my_arg1: my_arg2,
    }
    if value_1 == int() and value_2 == int():
        return my_arg1 - my_arg2
    if value_1 == str() and value_2 == str():
        return my_arg1 + my_arg2
    if value_1 == str() and value_2 != str():
        return my_dict
    else:
        return my_arg1, my_arg2


result_3 = my_function(1, 2)



















