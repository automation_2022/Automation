from personnel import Personnel
from datetime import date
from dataclasses import dataclass, field
from type_gender import GenderDefinition

@dataclass(order=True)
class Pupil(Personnel):
    name: str = field(compare=False)
    surname: str = field(compare=False)
    position: str = field(compare=False)
    expense: int = field(compare=False)
    marks: int = field(compare=False)
    benefit: int = field(compare=False)
    salary: int = field(compare=False)
    age: date = field(compare=False)
    sort_index: int = field(init=False, repr=False, compare=True)

    def __post_init__(self):
        if not isinstance(self.age, date):
            raise TypeError(
                f"Incorrect data type of 'birth_date' parameter: {type(self.age)}. Should be: datetime.date!")

        self.sort_index = self.age_determination

    @property
    def age_determination(self):
        return (date.today() - self.age).days // 365

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def calculate_average(self, total):
        scores = []
        for i in range(1, 6):
            score = int(input('Enter score {0}: '.format(i)))
            scores.append(score)
        total = sum(scores)
        school_average_mark = total / 5
        print('Average grade is: ' + str(school_average_mark))

    def reward(self):
        self.benefit += self.marks
        return self.marks

    def claim_pupils_gender(self):
        print(f"{Pupil.position} are {GenderDefinition.FEMALE} and {GenderDefinition.MALE} ")

